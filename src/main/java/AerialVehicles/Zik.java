package AerialVehicles;


import Capabilities.*;
import Entities.Coordinates;
import Enums.CameraType;
import Enums.CapabilitiesNames;
import Enums.SensorType;

import java.util.Map;

public class Zik extends Hermes {
    public Zik(Coordinates homeBase, SensorType sensorType, CameraType cameraType) {
        super(homeBase, Map.ofEntries(
                Map.entry(CapabilitiesNames.INTELLIGENCE.name() , new IntelligenceCapability(sensorType)),
                Map.entry(CapabilitiesNames.BDA.name() , new BdaCapability(cameraType))
        ));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}