package AerialVehicles;

import Capabilities.ICapability;
import Entities.Coordinates;

import java.util.Map;

public abstract class FightAirCraft extends AerialVehicle {
    public FightAirCraft(Coordinates homeBase, Map<String, ICapability> capabilities) {
        super(250, homeBase, capabilities);
    }
}

