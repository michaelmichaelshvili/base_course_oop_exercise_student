package Capabilities;

import Enums.SensorType;

public class IntelligenceCapability implements ICapability {
    private SensorType sensorType;

    public IntelligenceCapability(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public SensorType getSensorType() {
        return sensorType;
    }
}
