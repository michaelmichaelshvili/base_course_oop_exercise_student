package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.ICapability;
import Entities.Coordinates;

public abstract class Mission {
    protected Coordinates destination;
    protected String pilotName;
    protected AerialVehicle operatingAerialVehicle;

    public Mission(Coordinates destination, String pilotName) {
        this.destination = destination;
        this.pilotName = pilotName;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        operatingAerialVehicle.flyTo(destination);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        operatingAerialVehicle.land(operatingAerialVehicle.getHomeBase());
    }

    public void finish() {
        System.out.println(executeMission());
        operatingAerialVehicle.land(operatingAerialVehicle.getHomeBase());
        System.out.println("Finish Mission!");
    }

    protected void setOperatingAerialVehicle(AerialVehicle operatingAerialVehicle) {
        this.operatingAerialVehicle = operatingAerialVehicle;
    }

    protected boolean isPlaneSuitForMission(AerialVehicle plane, String... capabilitiesNames) {
        boolean isPlaneSuitForMission = true;
        for (String capabilityName : capabilitiesNames) {
            ICapability capability = plane.getCapability(capabilityName);
            if (capability == null) { // The plane doesn't have the capability
                isPlaneSuitForMission = false;
                break;
            }
        }
        return isPlaneSuitForMission;
    }

    protected abstract String executeMission();
}
