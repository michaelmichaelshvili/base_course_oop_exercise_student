package AerialVehicles;

import Capabilities.ICapability;
import Entities.Coordinates;

import java.util.Map;

public abstract class Hermes extends UnmannedAerialVehicle {
    public Hermes(Coordinates homeBase, Map<String, ICapability> capabilities) {
        super(100, homeBase, capabilities);
    }
}
