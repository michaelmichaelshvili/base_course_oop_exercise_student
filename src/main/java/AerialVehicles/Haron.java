package AerialVehicles;

import Capabilities.ICapability;
import Entities.Coordinates;

import java.util.Map;

public abstract class Haron extends UnmannedAerialVehicle {
    public Haron(Coordinates homeBase, Map<String, ICapability> capabilities) {
        super(150, homeBase, capabilities);
    }
}
