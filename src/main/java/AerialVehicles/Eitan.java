package AerialVehicles;

import Capabilities.AttackCapability;
import Enums.CapabilitiesNames;
import Capabilities.IntelligenceCapability;
import Entities.Coordinates;
import Enums.MissileType;
import Enums.SensorType;
import java.util.Map;

public class Eitan extends Haron {

    public Eitan(Coordinates homeBase, int missileCount, MissileType missileType, SensorType sensorType) {
        super(homeBase, Map.ofEntries(
                Map.entry(CapabilitiesNames.ATTACK.name(), new AttackCapability(missileCount, missileType)),
                Map.entry(CapabilitiesNames.INTELLIGENCE.name(), new IntelligenceCapability(sensorType))
        ));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
