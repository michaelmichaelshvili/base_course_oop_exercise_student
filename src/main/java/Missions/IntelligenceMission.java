package Missions;

import AerialVehicles.AerialVehicle;
import Enums.CapabilitiesNames;
import Capabilities.IntelligenceCapability;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle operatingAerialVehicle, String region) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName);

        if (!isPlaneSuitForMission(operatingAerialVehicle, CapabilitiesNames.INTELLIGENCE.name())) {
            throw new AerialVehicleNotCompatibleException(String.format("This aerial vehicle (%s) can not execute intelligence mission", operatingAerialVehicle));
        }

        setOperatingAerialVehicle(operatingAerialVehicle);
        this.region = region;
    }

    @Override
    protected String executeMission() {
        IntelligenceCapability capability = (IntelligenceCapability) operatingAerialVehicle.getCapability(CapabilitiesNames.INTELLIGENCE.name()); // we already know that the plane suits for this mission
        return String.format("%s: %s Collecting Data in %s with: sensor type: %s", pilotName, operatingAerialVehicle, region, capability.getSensorType());
    }
}
