package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.BdaCapability;
import Enums.CapabilitiesNames;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle operatingAerialVehicle, String objective) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName);

        if (!isPlaneSuitForMission(operatingAerialVehicle, CapabilitiesNames.BDA.name())) {
            throw new AerialVehicleNotCompatibleException(String.format("This aerial vehicle (%s) can not execute bda mission", operatingAerialVehicle));
        }

        setOperatingAerialVehicle(operatingAerialVehicle);
        this.objective = objective;
    }

    @Override
    protected String executeMission() {
        BdaCapability capability = (BdaCapability) operatingAerialVehicle.getCapability(CapabilitiesNames.BDA.name()); // we already know that the plane suits for this mission
        return String.format("%s: %s taking pictures of suspect %s with: %s camera", pilotName, operatingAerialVehicle, objective, capability.getCameraType());
    }
}
