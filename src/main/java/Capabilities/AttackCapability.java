package Capabilities;

import Enums.MissileType;

public class AttackCapability implements ICapability {
    private int missileCount;
    private MissileType missileType;

    public AttackCapability(int missileCount, MissileType missileType) {
        this.missileCount = missileCount;
        this.missileType = missileType;
    }

    public int getMissileCount() {
        return missileCount;
    }

    public MissileType getMissileType() {
        return missileType;
    }
}
