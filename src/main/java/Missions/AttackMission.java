package Missions;

import AerialVehicles.AerialVehicle;
import Capabilities.AttackCapability;
import Enums.CapabilitiesNames;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;


    public AttackMission(Coordinates destination, String pilotName, AerialVehicle operatingAerialVehicle, String target) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName);

        if (!isPlaneSuitForMission(operatingAerialVehicle, CapabilitiesNames.ATTACK.name())) {
            throw new AerialVehicleNotCompatibleException(String.format("This aerial vehicle (%s) can not execute attack mission", operatingAerialVehicle));
        }

        setOperatingAerialVehicle(operatingAerialVehicle);
        this.target = target;
    }

    @Override
    protected String executeMission() {
        AttackCapability capability = (AttackCapability) operatingAerialVehicle.getCapability(CapabilitiesNames.ATTACK.name()); // we already know that the plane suits for this mission
        return String.format("%s: %s Attacking suspect %s with %sX%d", pilotName, operatingAerialVehicle, target, capability.getMissileType(), capability.getMissileCount());
    }
}
