package Enums;

public enum CameraType {
    REGULAR, THERMAL, NIGHT_VISION
}
