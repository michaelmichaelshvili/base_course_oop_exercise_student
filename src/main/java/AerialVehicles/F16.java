package AerialVehicles;


import Capabilities.*;
import Entities.Coordinates;
import Enums.CameraType;
import Enums.CapabilitiesNames;
import Enums.MissileType;

import java.util.Map;

public class F16 extends FightAirCraft {
    public F16(Coordinates homeBase, int missileCount, MissileType missileType, CameraType cameraType) {
        super(homeBase, Map.ofEntries(
                Map.entry(CapabilitiesNames.ATTACK.name() , new AttackCapability(missileCount, missileType)),
                Map.entry(CapabilitiesNames.BDA.name() , new BdaCapability(cameraType))
        ));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
