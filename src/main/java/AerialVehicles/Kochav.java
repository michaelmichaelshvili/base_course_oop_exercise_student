package AerialVehicles;

import Capabilities.*;
import Entities.Coordinates;
import Enums.CameraType;
import Enums.CapabilitiesNames;
import Enums.MissileType;
import Enums.SensorType;

import java.util.Map;

public class Kochav extends Hermes {
    public Kochav(Coordinates homeBase, int missileCount, MissileType missileType, SensorType sensorType, CameraType cameraType) {
        super(homeBase, Map.ofEntries(
                Map.entry(CapabilitiesNames.ATTACK.name() , new AttackCapability(missileCount, missileType)),
                Map.entry(CapabilitiesNames.INTELLIGENCE.name() , new IntelligenceCapability(sensorType)),
                Map.entry(CapabilitiesNames.BDA.name() , new BdaCapability(cameraType))
        ));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
