import AerialVehicles.*;
import Entities.Coordinates;
import Enums.CameraType;
import Enums.MissileType;
import Enums.SensorType;
import Missions.*;

public class Main {
    public static void main(String[] args) {
        scenario1();
        scenario2();
        scenario3();
    }


    public static void scenario1() {
        System.out.println("\n\n*** scenario 1 - Successful attack in Iran ***");
        Coordinates destination = new Coordinates(32.0, 33.0);
        Coordinates homeBase = new Coordinates(34.0, 22.0);
        AerialVehicle eitan = new Eitan(homeBase, 4, MissileType.AMRAM, SensorType.INFRA_RED);
        AerialVehicle f15 = new F15(homeBase, 8, MissileType.PYTHON, SensorType.INFRA_RED);
        AerialVehicle zik = new Zik(homeBase, SensorType.ELINT, CameraType.NIGHT_VISION);
        try {
            Mission intelligenceMission = new IntelligenceMission(destination, "David", eitan, "Iran");
            intelligenceMission.begin();
            intelligenceMission.finish();
            Mission attackMission = new AttackMission(destination, "Michael", f15, "Iran");
            attackMission.begin();
            attackMission.finish();
            Mission bdaMission = new BdaMission(destination, "Shalom", zik, "Iran");
            bdaMission.begin();
            bdaMission.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }
    }

    public static void scenario2() {
        System.out.println("\n\n*** scenario 2 - Not suit plane for mission ***");
        Coordinates destination = new Coordinates(112.3, 54.4);
        Coordinates homeBase = new Coordinates(12.3, 34.7);
        AerialVehicle f15 = new F15(homeBase, 8, MissileType.PYTHON, SensorType.INFRA_RED);
        try {
            Mission bdaMission = new BdaMission(destination, "Shalom", f15, "Iran");
            bdaMission.begin();
            bdaMission.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void scenario3() {
        System.out.println("\n\n*** scenario 3 - Abort intelligence mission ***");
        Coordinates destination = new Coordinates(32.0, 33.0);
        Coordinates homeBase = new Coordinates(34.0, 22.0);
        UnmannedAerialVehicle kochav = new Kochav(homeBase, 6, MissileType.SPICE250, SensorType.ELINT, CameraType.NIGHT_VISION);
        try {
            Mission intelligenceMission = new IntelligenceMission(destination, "Moshe", kochav, "Gaza");
            intelligenceMission.begin();
            kochav.hoverOverLocation(destination);
            intelligenceMission.cancel();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }

    }
}
