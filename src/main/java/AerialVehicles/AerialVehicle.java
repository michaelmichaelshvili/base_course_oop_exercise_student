package AerialVehicles;


import Capabilities.ICapability;
import Entities.Coordinates;
import Enums.FlightStatus;

import java.util.Map;

public abstract class AerialVehicle {
    protected int flightHoursBeforeFix;
    protected int flightHoursFromLastFix;
    protected FlightStatus flightStatus;
    protected Coordinates homeBase;
    protected Map<String, ICapability> capabilities;

    public AerialVehicle(int flightHoursBeforeFix, Coordinates homeBase, Map<String, ICapability> capabilities) {
        this.flightHoursBeforeFix = flightHoursBeforeFix;
        this.flightHoursFromLastFix = 0;
        this.flightStatus = FlightStatus.READY;
        this.homeBase = homeBase;
        this.capabilities = capabilities;
    }

    public void flyTo(Coordinates destination) {
        if (flightStatus == FlightStatus.READY) {
            System.out.println("Flying to: " + destination);
            flightStatus = FlightStatus.IN_AIR;
        } else if (flightStatus == FlightStatus.NOT_READY) {
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on: " + destination);
        check();
    }

    private void check() {
        if (flightHoursFromLastFix >= flightHoursBeforeFix) { // need repair
            flightStatus = FlightStatus.NOT_READY;
            repair();
        } else {
            flightStatus = FlightStatus.READY;
        }
    }

    private void repair() {
        flightHoursFromLastFix = 0;
        flightStatus = FlightStatus.READY;
    }

    public Coordinates getHomeBase() {
        return homeBase;
    }

    public Map<String, ICapability> getCapabilities() {
        return capabilities;
    }

    public ICapability getCapability(String capabilityName) {
        if (capabilities.containsKey(capabilityName)) {
            return capabilities.get(capabilityName);
        }
        return null;
    }
}
