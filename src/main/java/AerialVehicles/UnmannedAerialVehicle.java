package AerialVehicles;

import Capabilities.ICapability;
import Entities.Coordinates;
import Enums.FlightStatus;

import java.util.Map;

public abstract class UnmannedAerialVehicle extends AerialVehicle{

    public UnmannedAerialVehicle(int flightHoursBeforeFix, Coordinates homeBase, Map<String, ICapability> capabilities) {
        super(flightHoursBeforeFix, homeBase, capabilities);
    }

    public String hoverOverLocation(Coordinates destination){
        flightStatus = FlightStatus.IN_AIR;
        String hoverString = "Hovering Over: " + destination;
        System.out.println(hoverString);
        return hoverString;
    }
}
