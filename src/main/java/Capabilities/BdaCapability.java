package Capabilities;

import Enums.CameraType;

public class BdaCapability implements ICapability{

    private CameraType cameraType;

    public BdaCapability(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public CameraType getCameraType() {
        return cameraType;
    }
}
